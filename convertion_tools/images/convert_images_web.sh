#!/bin/bash

function print_help { echo -e "
USAGE:
  ./convert_images_web.sh <path_to_img_dir> <pattern_for_imgs>

EXAMPLES:
  # Convert all images in a particular image directory:
  ./convert_images_web.sh WA201204/r20120421_040204_rottnest_05_25m_s_out/images/ *_LC16.png

  # Convert all image directories in ALL dives contained in a campaign named WA201204:
  ls -d WA201204/r*/images/ | xargs -I {} ./convert_images_web.sh {} *_LC16.png
  ls -d WA201204/r*/images/ | xargs -P 8 -I {} ./convert_images_web.sh {} *_LC16.png  # multi-threaded!

DEPENDS ON:
  mogrify # found in imagemagick package, available through apt/brew/etc..

CREATED:
  03/03/2017, Ariell Friedman, Greybits Engineering (ariell@greybits.com.au)
"
exit 1
}

# Print help if < 2 arguments are provided
if [[ $# -lt 2 ]] ; then print_help; fi

IMG_DIRNAME="$1"                                # <path_to_img_dir>
IMG_PATTERN="$2"                                # <pattern_for_imgs>
BASE_DIRNAME=`dirname $IMG_DIRNAME`             # base directory to save files
WEBTHM_DIRNAME="$BASE_DIRNAME/web_thumbnails/"  # dir to save web_thumbnails
WEBIMG_DIRNAME="$BASE_DIRNAME/web_images/"      # dir to save web_images

THM_FRMT="jpg"        # format for web_thumbnails
THM_QLTY="80"         # quality for web_thumbails
THM_SIZE="300x300"    # max dimensions for web_thumbnails (keeps proportions)
IMG_FRMT="jpg"        # format for web_images
IMG_QLTY="90"         # quality for web_images

echo "Converting thumbnails..."
mkdir $WEBTHM_DIRNAME
mogrify -resize $THM_SIZE -quality $THM_QLTY -format $THM_FRMT -path $WEBTHM_DIRNAME $IMG_DIRNAME/$IMG_PATTERN

echo "Converting images..."
mkdir $WEBIMG_DIRNAME
mogrify -quality $IMG_QLTY -format $IMG_FRMT -path $WEBIMG_DIRNAME $IMG_DIRNAME/$IMG_PATTERN
