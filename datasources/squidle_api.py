import requests
import json


class SimpleSQAPI(object):
    def __init__(self, domainurl, api_token, platform_id=None, user_id=None):
        self.url_patterns = {
            "platform": domainurl+"/api/platform",
            "campaign": domainurl+"/api/campaign",
            "deployment": domainurl+"/api/deployment",
            "upload_media": domainurl+"/api/media/save",
            "get_campaign": domainurl+'/api/campaign?q={{"filters":[{{"name":"key","op":"==","val":"{campaign_key}"}},{{"name":"user_id","op":"==","val":"{user_id}"}}],"single":true}}',
            "get_deployment": domainurl+'/api/deployment?q={{"filters":[{{"name":"key","op":"==","val":"{deployment_key}"}},{{"name":"campaign_id","op":"==","val":"{campaign_id}"}}],"single":true}}',
            "get_user": domainurl+'/api/users?q={{"filters":[{{"name":"api_token","op":"eq","val":"'+api_token+'"}}],"single":true}}',
            "get_platform": domainurl+'/api/platform?q={{"filters":[{{"name":"name","op":"==","val":"{platform_name}"}}],"single":true}}',
            "get_last_deployment": domainurl + '/api/deployment?q={{"order_by":[{{"field":"created_at","direction":"desc"}}],"limit":1,"single":true}}',
            "get_last_campaign": domainurl + '/api/campaign?q={{"order_by":[{{"field":"created_at","direction":"desc"}}],"limit":1,"single":true}}'
        }
        self.api_token = api_token
        self.user_id = user_id
        self.platform_id = platform_id
        self.domainurl = domainurl

        try:
            if user_id is None:   # if user id is not passed in, auto set based on api_token
                self.get_user()
        except Exception as e:
            print ("*** WARNING: Was not able to atumatically get user_id ({})".format(e))


    def get_user(self):
        user = self.make_api_request(self.build_url("get_user"))
        self.user_id = user['id']   # set property
        return user

    def get_campaign(self, campaign_key, user_id):
        return self.make_api_request(self.build_url("get_campaign", user_id=str(user_id), campaign_key=campaign_key))

    def get_platform(self, platform_name):
        platform = self.make_api_request(self.build_url("get_platform", platform_name=platform_name))
        self.platform_id = platform["id"]  # set property
        return platform

    def get_deployment(self, deployment_key, campaign_id):
        return self.make_api_request(self.build_url("get_deployment", campaign_id=str(campaign_id), deployment_key=deployment_key))

    def get_last_deployment(self):
        return self.make_api_request(self.build_url("get_last_deployment"))

    def get_last_campaign(self):
        return self.make_api_request(self.build_url("get_last_campaign"))

    def new_campaign(self, name, campaign_key, platform_id=None, user_id=None):
        user_id = self.user_id if user_id is None else user_id
        platform_id = self.platform_id if platform_id is None else platform_id
        data = {
            'name': name,
            'key': campaign_key,
            'user_id': user_id,
            'platform_id': platform_id
        }
        return self.make_api_request(self.build_url("campaign"), "post", data=data)

    def new_deployment(self, name, deployment_key, campaign_id, user_id=None):
        user_id = self.user_id if user_id is None else user_id
        data = {
            'name': name,
            'key': deployment_key,
            'user_id': user_id,
            'campaign_id': campaign_id
        }
        return self.make_api_request(self.build_url("deployment"), "post", data=data)

    def new_media_item(self, pose, timestamp, posedata, deployment_id=None, path=None, key=None, path_thm=None,  media_type="image"):
        data = {
            "path": path,
            "pose": pose,
            "timestamp": timestamp,
            "posedata": posedata,
            "media_type": media_type
        }
        if key is not None:
            data['key'] = key
        if path is not None:
            data['path'] = path
        if path_thm is not None:
            data['path_thm'] = path_thm
        if deployment_id is not None:
            data['deployment_id'] = deployment_id
        return self.make_api_request(self.build_url("upload_media"), "post", data=data)

    def build_url(self, key, **params):
        return self.url_patterns[key].format(**params)

    def make_api_request(self, url, method="get", data=None):
        if method == "post":
            headers = {'Content-type': 'application/json', 'Accept': 'text/plain', 'auth-token': self.api_token}
            response_json = requests.post(url, data=json.dumps(data), headers=headers).json()
        elif method == "get":
            response_json = requests.get(url).json()
        if 'id' not in response_json:  # TODO: also check response code
            raise Exception("ERROR: API result not found!")
        return response_json

