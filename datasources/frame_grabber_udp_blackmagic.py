# import sys
# sys.path.insert(0, "/usr/local/bin")
from __future__ import print_function

import gi
import sys

gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
GObject.threads_init()
Gst.init(None)

import argparse
import json
import os
import shutil
import threading
from socket import socket, AF_INET, SOCK_DGRAM
import select
from time import sleep, time
import datetime
import tempfile

from flask import Flask, request, jsonify, render_template

# import openvdm
# from pymongo import MongoClient
from squidle_api import SimpleSQAPI

# Prevent logging for Flask
import logging
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


DEBUG = False  # Global Debug Flag
SAVE = True    # Global to decide whether or not to post frames+data to sq+
JPEG_QUALITY = 85  # JPEG quality factor in the range 0-100
MAX_FILES_TO_STORE = 10  # number of files to preserve in capture directory

DEFAULT_SERVER_PORT = 8085  # default  port for HTTP server
DEFAULT_UDP_PORTS = [10000]  # default ports to listen to for UDP messages
DEFAULT_UDP_JANITOR_TIMEOUT = 5*60   # timeout for UDP feed to automatically stop capture
UDP_JANITOR_DELAY = 5       # delay in s to check UDP status
UDP_JANITOR_TIMEOUT_WARNING = 30    # time to generate warning message

STATE_STARTED = "running"
STATE_STOPPED = "stopped"
STATE_INITIALISING = STATE_STOPPED
STATE_ERROR = STATE_STOPPED

parser = argparse.ArgumentParser(description='Frame capture and data logger for Squidle+')
parser.add_argument('-l', '--listen_ports', nargs='+', type=int, default=DEFAULT_UDP_PORTS, help='Ports to listen on')
parser.add_argument('-p', '--server_port', default=DEFAULT_SERVER_PORT, type=int, help='Port to start webserver')
parser.add_argument('-d', '--debug', action='store_true', help='Show debug messages')
parser.add_argument('-u', '--udp_timeout', default=DEFAULT_UDP_JANITOR_TIMEOUT, type=int, help='Timeout for UDP feed to automatically stop capture (in seconds)')
parser.add_argument('-t', '--test', action='store_true', help='Run in test mode')
parser.add_argument('-n', '--no_post', action='store_true', help='Do not post to SQUIDLE+. Files will be saved but not logged in DB.')


settings = {
    "SQUIDLE_ROOT_URL": 'http://127.0.0.1:80',  # 'http://10.23.14.51:80'
    "SQUIDLE_API_KEY": '601f41fec803886de808a083deb573d7b5fbd24d90a829e9908ba326',
    "DIRBASE": "/mnt/nfs/CruiseData-link/",
    "URLBASE": "http://10.23.10.205/media/",
    "FILEPATH": "{campaign_key}/Subastian/{deployment_key}/Squidle_Framegrabs/{campaign_key}_{deployment_key}_{timestamp}_S5K.jpg"
}


# Turn on GStreamer's debug messages
Gst.debug_set_active(True)
Gst.debug_set_default_threshold(1) # Threshold 1 = errors only,

# Instantiate webserver
#template_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), "templates")
#print(template_folder)
app = Flask(__name__ )  #, template_folder=template_folder)

# # Create the OpenVDM object
# OVDM = openvdm.OpenVDM()

# Create the SQAPI object
sq = SimpleSQAPI(settings["SQUIDLE_ROOT_URL"], settings["SQUIDLE_API_KEY"])

# Setup connection to the MongoDB
# client = MongoClient()
# db = client.squidleIntegration
# coreVars = db.coreVars
# metadata = db.metadata

# Framegrabber status
state = {
    "status": STATE_INITIALISING,
    "status_message": "Initialising...",
    "deployment_id": None,
    "deployment_key": None,
    "campaign_key": None,
    "error_udp": None,
    "error_framegrabber": None
}

# initialise the variables:
pose = {
    "lat": None,
    "lon": None,
    "dep": None,
    "alt": None
}
posedata = {
    "roll": None,
    "pitch": None,
    "heading": None,
    "temperature": None,
    "salinity": None,
    "conductivity": None,
    "sound_vel": None,
    "oxygen": None,
    "oxygen_temp": None
}
udp_message_time = None
udp_is_valid_pose = False
current_frame = ""

# def get_deployment_id():
#     deployment_id_record = coreVars.find_one({'name': 'deployment_id'})
#     return deployment_id_record['value'] if deployment_id_record else None


# -------------------------------------------------------------------------------------
# GStreamer error message callback
# -------------------------------------------------------------------------------------
def error_cb(bus, msg, loop):
    global state
    state["error_framegrabber"] = "GStreamer callback error: {}".format(msg.parse_error()[0])
    eprint(state["error_framegrabber"])


# -------------------------------------------------------------------------------------
# GStreamer element message callback
# -------------------------------------------------------------------------------------
def element_cb(bus, msg, loop):
    global state, pose, posedata, SAVE, current_frame

    if state["status"] == STATE_STARTED and state["deployment_id"] is not None:
        ts = datetime.datetime.utcnow()
        sq_timestamp = ts.strftime('%Y-%m-%d %H:%M:%S.%f')  # timestamp formatted %Y-%m-%d %H:%M:%S.%f
        fname_timestamp = ts.strftime('%Y%m%dT%H%M%S%f')

        # generate a timestamped filename
        tmp_fname = msg.get_structure().get_value('filename')
        fpath = os.path.join(settings["DIRBASE"], settings["FILEPATH"]).format(
            campaign_key=state["campaign_key"], deployment_key=state["deployment_key"], timestamp=fname_timestamp
        )
        url = os.path.join(settings["URLBASE"], settings["FILEPATH"]).format(
            campaign_key=state["campaign_key"], deployment_key=state["deployment_key"], timestamp=fname_timestamp
        )
        dprint("SAVING: {}\nURL: {}".format(fpath, url))

        # copy and rename the framegrab
        current_frame = url
        try:
            if not os.path.isdir(os.path.dirname(fpath)):
                os.makedirs(os.path.dirname(fpath))
            shutil.copyfile(tmp_fname, fpath)
            shutil.chown(fpath, 134219956, 134219956)
        except Exception as error:
            state["error_framegrabber"] = "Couldn't move framegrab: {} to {} ({})".format(tmp_fname, fpath, error)
            eprint(state["error_framegrabber"])
        else:
            try:
                dprint("Saving: sq.new_media_item\n pose={}\n  posedata={}\n timestamp={}\n path={}\n deployment_id={}".format(
                    json.dumps(pose), json.dumps(posedata), sq_timestamp, url, state["deployment_id"]
                ))
                if SAVE:
                    results = sq.new_media_item(pose, sq_timestamp, posedata, path=url, deployment_id=state["deployment_id"])
                    state["error_framegrabber"] = None
            except Exception as error:
                state["error_framegrabber"] = "Error makeing API call: new_media_item @{} ({})".format(sq_timestamp, error)
                eprint(state["error_framegrabber"])


def start_frame_grabber(test=False):

    # Create the directory to temporarily store the images
    tprint("Starting main framegrabber loop...")
    CAPTURE_DIR = tempfile.mkdtemp()
    dprint("Temporary capture location: {}".format(CAPTURE_DIR))

    # Create GObject Main Loop
    loop = GObject.MainLoop()

    # Create gstreamer pipeline
    pipe = Gst.Pipeline()

    # Create elements
    capsfilter = Gst.ElementFactory.make('capsfilter', None)
    videoconvert = Gst.ElementFactory.make('videoconvert', None)
    videorate = Gst.ElementFactory.make('videorate', None)
    jpegenc = Gst.ElementFactory.make('jpegenc', None)
    multifilesink = Gst.ElementFactory.make('multifilesink', None)

    # Set Video Source
    if test:
        tprint("*** RUNNING IN TEST MODE: Setting video source to INTERNAL TEST SIGNAL")
        decklinkvideosrc = Gst.ElementFactory.make('videotestsrc', None)
        capsfilter.set_property('caps', Gst.Caps.from_string('video/x-raw,width=3840,height=2160,framerate=1/1'))
    else:
        tprint("Setting video source to BMD DeckLink Video Source")
        decklinkvideosrc = Gst.ElementFactory.make('decklinkvideosrc', None)
        # decklinkvideosrc.set_property('connection', 'hdmi')   #don't setting this because it already defaults to auto-select between HDMI and SDI inputs
        capsfilter.set_property('caps', Gst.Caps.from_string('video/x-raw,framerate=1/1'))

    # Set propoerties
    jpegenc.set_property('quality', JPEG_QUALITY)
    multifilesink.set_property('sync', True)
    multifilesink.set_property('location', CAPTURE_DIR + '/S5K%05d.jpg')
    multifilesink.set_property('max-files', MAX_FILES_TO_STORE)
    multifilesink.set_property('post-messages', True)

    # Add elements
    pipe.add(decklinkvideosrc)
    pipe.add(videoconvert)
    pipe.add(videorate)
    pipe.add(capsfilter)
    pipe.add(jpegenc)
    pipe.add(multifilesink)

    # link gstreamer elements
    decklinkvideosrc.link(videoconvert)
    videoconvert.link(videorate)
    videorate.link(capsfilter)
    capsfilter.link(jpegenc)
    jpegenc.link(multifilesink)

    # run gstreamer pipeline
    while True:
        retval = pipe.set_state(Gst.State.PLAYING)
        if retval == Gst.StateChangeReturn.SUCCESS:
            break  # capture card signal present
        tprint("Waiting to start pipline but capture signal not present...")
        sleep(5)  # wait for capture card signal

    bus = pipe.get_bus()
    bus.add_signal_watch()
    bus.connect("message::error", error_cb, loop)
    bus.connect("message::element", element_cb, loop)

    try:
        tprint("Starting GEvent loop...")
        loop.run()
    except KeyboardInterrupt:
        tprint("Quitting GEvent loop...")
        loop.quit()

    tprint("Cleaning up...")
    # Cleanup gstreamer pipeline
    pipe.set_state(Gst.State.NULL)

    # Delete the temporary capture dir
    shutil.rmtree(CAPTURE_DIR)


def get_udp_feed(ports):
    global pose, posedata, state, udp_message_time, udp_is_valid_pose
    sockets = []
    for p in ports:
        sockets.append(socket(AF_INET, SOCK_DGRAM))
        sockets[-1].bind(('', p))

    dprint("Started UDP thread, watching ports: {}...".format(ports))

    state["error_udp"] = "Waiting for SPRINT, O2 and CTD messages..."

    while True:
        try:
            readable, writable, exceptional = select.select(sockets, [], sockets)
            for s in readable:
                data, addr = s.recvfrom(1024)
                params = data.decode().split(",")

                dprint("UDP MESSAGE: {}".format(params))

                msg_type = params[0]
                if msg_type == "$SPRINT":
                    udp_is_valid_pose = bool(parse_float(params[7]))
                    is_valid_orientation = bool(parse_float(params[4]))

                    pose["lat"] = parse_float(params[5], udp_is_valid_pose)
                    pose["lon"] = parse_float(params[6], udp_is_valid_pose)
                    pose["dep"] = parse_float(params[13], udp_is_valid_pose)
                    pose["alt"] = parse_float(params[11], udp_is_valid_pose)
                    posedata["roll"] = parse_float(params[1], is_valid_orientation)
                    posedata["pitch"] = parse_float(params[2], is_valid_orientation)
                    posedata["heading"] = parse_float(params[3], is_valid_orientation)

                    if b'SPRINT,,,,,,,,,,,,,' not in data:  # only update time if non-empty sprint message is received
                        udp_message_time = time()

                elif msg_type == "$CTD":
                    posedata["temperature"] = parse_float(params[2])
                    posedata["salinity"] = parse_float(params[5])
                    posedata["conductivity"] = parse_float(params[1])
                    posedata["sound_vel"] = parse_float(params[6])

                elif msg_type == "$O2":
                    posedata["oxygen"] = parse_float(params[1])
                    posedata["oxygen_temp"] = parse_float(params[3])

                #if b'SPRINT,,,,,,,,,,,,,' in data and state["status"] == STATE_STARTED:  # if empty sprint message, assume ROV is off and not working, so disable SQUIDLE logger
                #    state["status"] = STATE_STOPPED
                #    state["status_message"] = "Capture STOPPED AUTOMATICALLY triggered by empty UDP SPRINT message."
        except Exception as e:
            state["status"] = STATE_STOPPED
            state["error_udp"] = "ERROR WITH NAV FEED: {}. Check status of UDP feed and configuration of ip/ports.".format(e)
            eprint(state["error_udp"])


@app.route("/framegrabber/<action>", methods=["GET", "POST"])
def handle_http_requests(action):
    global state
    params = request.args.to_dict()  # deployment_id, platform_id, period
    remote_addr = params.get("remote_addr", request.remote_addr)
    if action == "start":
        deployment = sq.get_last_deployment()
        # state["deployment_id"] = get_deployment_id()
        # state["campaign_key"] = OVDM.getCruiseID()
        # state["deployment_key"] = OVDM.getLoweringID()
        state["deployment_id"] = deployment["id"]
        state["deployment_key"] = deployment["key"]
        state["campaign_key"] = deployment["campaign"]["key"]

        state["status"] = STATE_STARTED
        state["status_message"] = "Framegrabber STARTED by user on: {}".format(remote_addr)
        tprint(state["status_message"])
    elif action == "stop":
        state["status"] = STATE_STOPPED
        state["status_message"] = "Framegrabber STOPPED by user on: {}".format(remote_addr)
        tprint(state["status_message"])
    elif action == "data":
        return jsonify({"pose": pose, "posedata": posedata, "udp_message_time": udp_message_time, "current_time": time(), "last_frame": current_frame})
    elif action == "settings":
        return render_template("frame_grabber_udp-settings.html", settings=settings, action="http://{}/framegrabber/settings".format(request.host))

    # status = "running" if
    return jsonify(state)


def start_server(port):
    tprint("Starting webserver on port: {}...".format(port))
    app.run(host='0.0.0.0', port=port, threaded=True)


def start_thread(target, args=(), kwargs=None):
    if kwargs is None:
        kwargs = {}
    t = threading.Thread(target=target, args=args, kwargs=kwargs)
    t.daemon = True
    t.start()
    return t


def parse_float(strfloat, is_valid=True):
    return float(strfloat.strip()) if strfloat.strip() and is_valid else None


def eprint(pstring):
    print("*** ERROR: {}: {}".format(datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'), pstring))


def dprint(pstring):
    global DEBUG
    if DEBUG:
        print("*** DEBUG: {}".format(pstring))


def tprint(pstring):
    print ("{}: {}".format(datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'), pstring))


def udp_janitor(timeout):
    global udp_is_valid_pose, udp_message_time
    while True:
        if udp_message_time is None:
            state["status"] = STATE_STOPPED
            state["status_message"] = "Capture STOPPED AUTOMATICALLY. No UDP SPRINT message"
            state["error_udp"] = "Capture STOPPED AUTOMATICALLY. Waiting for SPRINT, O2 and CTD over UDP..."
        else:
            if state["status"] == STATE_STARTED:
                delay = time() - udp_message_time
                #print (delay, timeout, UDP_JANITOR_TIMEOUT_WARNING)
                if delay > timeout:
                    state["status"] = STATE_STOPPED
                    state["status_message"] = "Capture STOPPED AUTOMATICALLY. No UDP SPRINT message for {:.0f}s".format(timeout)
                    udp_message_time = None
                elif delay > UDP_JANITOR_TIMEOUT_WARNING:
                    state["error_udp"] = "WARNING: SPRINT message is {:.0f}s old.".format(delay)
                    eprint(state["error_udp"])
                elif not udp_is_valid_pose:
                    state["error_udp"] = "WARNING: SPRINT is reporting an invalid pose."
                    eprint(state["error_udp"])
                else:
                    state["error_udp"] = None
            else:
                state["error_udp"] = None
        sleep(UDP_JANITOR_DELAY)


def main(listen_ports, server_port, debug, test, no_post, udp_timeout):
    global DEBUG, SAVE

    if debug:
        DEBUG = True
        tprint("Running in DEBUG MODE...")

    if no_post:
        SAVE = False
        tprint("*** Running in NO-POST mode: NOT SAVING TO SQUIDLE+ ***")

    # Start webserver thread
    start_thread(start_server, args=(server_port,))

    # Start UDP data thread
    start_thread(get_udp_feed, args=(listen_ports,))

    # Start UDP janitor to check message validity
    start_thread(udp_janitor, args=(udp_timeout,))

    # Start grabbing frames in main thread
    start_frame_grabber(test=test)


if __name__ == "__main__":
    args = parser.parse_args()
    main(args.listen_ports, args.server_port, args.debug, args.test, args.no_post, args.udp_timeout)
