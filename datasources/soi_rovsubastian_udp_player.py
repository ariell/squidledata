import argparse
import threading
import time
import socket

DEFAULT_UDP_IP = "127.0.0.1"
DEFAULT_UDP_PORT = 10000
DEFAULT_DELAY = 1
DEFAULT_LOGFILES = [
    "/home/mt/rov_data/test_dive/ROV_Sprint/ROV-SPRINT.Raw",
    "/home/mt/rov_data/test_dive/ROV_O2/ROV-O2.Raw",
    "/home/mt/rov_data/test_dive/ROV_CTD/ROV-CTD.Raw"
]


def logfile2udp(logfile, sock, delay=1, port=DEFAULT_UDP_PORT, ip=DEFAULT_UDP_IP):
    while True:
        with open(logfile) as f:
            for line in f:
                MESSAGE = line[24:]+'\r\n'
                print(MESSAGE)
                sock.sendto(bytes(MESSAGE, "utf-8"), (ip, port))
                time.sleep(delay)


def start_thread(target, args=(), kwargs=None):
    if kwargs is None:
        kwargs = {}
    t = threading.Thread(target=target, args=args, kwargs=kwargs)
    t.daemon = True
    t.start()
    return t


def main(ip, port, logfiles, delay):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    for f in logfiles:
        start_thread(logfile2udp, args=(f, sock,), kwargs=dict(delay=delay, ip=ip, port=port))

    while True:
        time.sleep(5)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="ROV log player for Leigton's logs")
    parser.add_argument('-i', '--ip', default=DEFAULT_UDP_IP, type=str, help='IP to send UDP messages')
    parser.add_argument('-p', '--port', default=DEFAULT_UDP_IP, type=int, help='Port to send UDP messages')
    parser.add_argument('-l', '--log_files', nargs='+', type=str, default=DEFAULT_LOGFILES, help='Ports to listen on')
    parser.add_argument('-d', '--delay', default=DEFAULT_DELAY, type=float, help='Delay in s to send messages')

    args = parser.parse_args()

    main(args.ip, args.port, args.log_files, args.delay)
