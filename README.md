# README #

Scripts for processing data for import into SQUIDLE+

## Image processing ##

Scripts to process images into a web-viewable format.

### Usage ###

```
#!bash

./process-images/convert_images_web.sh <path_to_img_dir> <pattern_for_imgs>
```  

Creates new directories ```web_thumbnails``` and ```web_images``` next to ```path_to_img_dir``` with converted images.

### Examples ###

```
#!bash

# Convert all images in a particular image directory:
./process-images/convert_images_web.sh WA201204/r20120421_040204_rottnest_05_25m_s_out/images/ *_LC16.png

# Convert all image directories in ALL dives contained in a campaign named WA201204:
ls -d WA201204/r*/images/ | xargs -I {} ./process-images/convert_images_web.sh {} *_LC16.png
```

### External dependencies ###
  mogrify # found in imagemagick package, available through apt/brew/etc..

## Mosaic processing ##

Example prototype interface: http://203.101.232.29/static/maps/demo-map.html

Plan is to integrate that into explore interface and include a mosaic for each 
applicable dive shown in here: http://203.101.232.29/geodata/explore

### Usage ###

```
./process-mosaic/bash/tile_mosaic.sh <path_to_mosaic_vrt>
```

Converts a mosaic vrt file and turns it into a TMS pyramid-tiled directory structure that can be viewable on the web.

### TODOs: WIP ###
Some of what is close but not quite right - there is a step that attempts to correct the projection for the mosaic before feeding it into the tiler. I have also included a multi-threaded version of the gdal2tiles.py file (similar to the one that is packaged with gdal).

Once we get that going for a mosaic, we can blast it through all the relevant dives and have them showing up on the map. 
It takes a while, so it may be a good idea to start off with some small ones to get the process right.

I still plan to write a little HTML viewer that will make it easier to see if things are working properly.


## SimpleAPI - realtime GUI ##

For convenience, I have created a standalone lib that will help with the integration.
You will need to import the SimpleSQAPI module. 

Note: squidle_api.py will need to be in your python path, and you will need the ``requests`` module installed.

```
#!python
from squidle_api import SimpleSQAPI
sq = SimpleSQAPI("http://{IP_OF_SQUIDLE_SERVER}:{PORT}", "{SQUIDLE_API_KEY}")
```

Where ``{IP_OF_SQUIDLE_SERVER}`` & ``{PORT}`` make up the URL for the SQ+ server and ``{SQUIDLE_API_KEY}`` is the API
key for the SQ+ user that will be running the script. We will create a user for OpenVDM.

To create a new campaign (start of cruise event trigger):

```
#!python
sq.get_platform("{PLATFORM_NAME}")                  # set the platform id
campaign = sq.new_campaign(name, campaign_key)      # create a new campaign / cruise
campaign_id = campaign["id"]                        # You will need to keep track of this for creating deployments
```

where ``{PLATFORM_NAME}`` is the name of the ROV in SQ+ - still to be created.
``name`` and ``key`` are the name and key of the campaign.
``key`` is typically a unique string without spaces or strange characters, and the name can be something more readable.
But... for simplicity can just be set to be the same.

To create a new deployment (lowering event trigger):
```
#!python
deployment = sq.new_deployment(name, deployment_key, campaign_id)       # POST new deployment using campaign_id from above
```

Save new media item (capturing frame grab event trigger):
```
#!python
# initialise the variables:
pose = {
    "lat":latitude,
    "lon":longitude,
    "dep":depth,
    "alt":altitude
}
pose_data = {
    "roll":roll,
    "pitch":pitch,
    "heading":heading
}
timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')   # timestamp formatted %Y-%m-%d %H:%M:%S.%f

# POST a new framegrab
sq.new_media_item(pose, timestamp, pose_data, path=http_path_to_image_file)
```

There are many more options and input to the functions that will give more flexibility, but this is the minimum spec
and what I think will be quickest to implement.

## Video processing ##
Watch this space

## Other data types processing ##
Watch this space